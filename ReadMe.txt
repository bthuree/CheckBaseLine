This script will compare an basline file with the reality (actually installed packages).

Result will look something like this.

#NOK: ERICfmm         - Upgrade to R8D05_EC03 (Currently at R8F01)
#NOK: ERICombsc       - Upgrade to R4A01 (Currently at R2D01)
#NOK: ERICurttr       - Upgrade to R5B03_EC11 (Currently at R5B03_EC05)
# OK: ERICurttr       - Later EC in system R5B03_EC05 (Base revision at R5A03_EC10)
#NOK: ERICurttr       - Upgrade to R5B03_EC11 (Currently at R5B03_EC05)



The baseline file looks like the below
#  ERICusck R5F01
#  ERICdmr R1H02_EC02


Options: -v, --version                  print version information and exit
        --versionnumber                 print ONLY version number and exit
         -h, --help                     print this help and exit
         -o, --outputfile FILE          where the result should go (default to screen)
         -b, --basefile FILE            Fetch input traps from file instead of from ovdumpevents
         -q, --quiet                    Only display errors
EOCAT

        echo "Sample:"
        echo "  `basename $0` -b /tmp/R4J_Base"
        echo "  `basename $0` -b /tmp/R4J_Base -q"
        echo ""
        exit 0
