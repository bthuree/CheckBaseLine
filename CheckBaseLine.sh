#!/bin/sh

# Input parameter is the Base Line file
# This base line file should consist <pkg> <rev>
#
#  ERICusck R5F01
#  ERICdmr R1H02_EC02

CHECK_BASELINE_VERSION=0.1

ALLINSTALLEDPKG=/tmp/InstalledPkgs.$$

NOK="NOK:"
OK=" OK:"

usage() {
        echo ""
        if [ "$1" = -h ]
        then
                exec 1>&2
                echo "U\c"
        else
                echo "u\c"
        fi

        echo "sage:     `basename $0`  -b <base file> [ options ] "
        echo ""
    
        echo "`basename $0` is a utility to compare the OSS package versions against a baseline file"


        cat <<EOCAT

Options: -v, --version                  print version information and exit
        --versionnumber                 print ONLY version number and exit
         -h, --help                     print this help and exit
         -o, --outputfile FILE          where the result should go (default to screen)
         -b, --basefile FILE            Fetch input traps from file instead of from ovdumpevents
         -q, --quiet                    Only display errors
EOCAT

        echo "Sample:"
        echo "  `basename $0` -b /tmp/R4J_Base"
        echo "  `basename $0` -b /tmp/R4J_Base -q"
        echo ""
        exit 0
}

Check_Cmd_Line() {

        { # Check command line parameters       while [ $# -ne 0 ]
        while [ $# -ne 0 ]
        do
                case "$1" in
                        -v|--version)
                                echo ""
                                echo "`basename $0` version ${CHECK_BASELINE_VERSION}" \
                                "by Bengt Thuree"
                                echo ""
                                exit 0
                                ;;
                        --versionnumber)
                                echo "${CHECK_BASELINE_VERSION}"
                                exit 0
                                ;;
                        -h|--help)
                                usage -h
                                ;;
                        -o|--outputfile)
                                shift
                                _OUTPUT_FILE_NAME="$1"
                                ;;
                        -b|--basefile)
                                shift
                                _BASE_FILE_NAME="$1"
                                ;;
                        -q|--quiet)
                                _QUIET="YES"
                                ;;
                        *)
                                echo ""
                                echo "`basename $0`: UNKNOWN COMMAND/OPTION" 2>&1
                                echo ""
                                usage
                                exit 1
                                ;;
                esac
                shift
        done
        
        if [ "x${_BASE_FILE_NAME}" = "x" ]; then
                echo ""
                echo "${BASENAME}: Missing base file" 2>&1
                echo ""
                usage
                exit 1
        fi
        
        } # End checking command line parameters
}

NiceOutput() {
        echo "$1:$2:$3" | awk -F: '{printf "%3s: %-15s - %s\n", $1, $2, $3}'
}

NiceOutput_Error() {
        NiceOutput "NOK" "$1" "$2"
}

NiceOutput_Info() {
        if [ "x${_QUIET}" = "x" ]; then
                NiceOutput "OK" "$1" "$2"
        fi
}

NiceOutput_OK() {
        if [ "x${_QUIET}" = "x" ]; then
                NiceOutput "OK" "$1" "Correct revision"
        fi
}                               
                                
GetAllInstalledPackages() {
        pkginfo > ${ALLINSTALLEDPKG}
}

get_newest_version ()
{
        # check if the label is longer if it is then the package has
        # upped a revision e.g. R1Z01 could become R1AA01
        # otherwise we just check to see which is newest via
        # their alphanumeric status.
		I_PKGNAME=`echo $1 | awk -F_ '{print $1}'`
		B_PKGNAME=`echo $2 | awk -F_ '{print $1}'`
		
        NR1=`echo ${I_PKGNAME} | wc -c`
        NR2=`echo ${B_PKGNAME} | wc -c`
        
        if [ ${NR2} -gt ${NR1} ]; then
                _RESULT="$2"
        else
                latest=`echo "$1\n$2\n" | sort | tail -1`
                _RESULT="${latest}"
        fi
}

get_latest()
{
#NOK: ERICfmm         - Upgrade to R8D05_EC03 (Currently at R8F01)
#NOK: ERICombsc       - Upgrade to R4A01 (Currently at R2D01)
#NOK: ERICurttr       - Upgrade to R5B03_EC11 (Currently at R5B03_EC05)
# OK: ERICurttr       - Later EC in system R5B03_EC05 (Base revision at R5A03_EC10)
#NOK: ERICurttr       - Upgrade to R5B03_EC11 (Currently at R5B03_EC05)
#1. no EC, then do below
#2. both EC, then do below
#3. One EC, then check base version		
        installed=$2
        base=$1
        get_newest_version "${installed}" "${base}"
        newestpkg="${_RESULT}"
        unset _RESULT
        if [ "$base" = "$installed" -o "$newestpkg" = "$base" ]; then
                _RESULT=${base}
        else
                _RESULT=${installed}
        fi
}

CompareAllBasePackages() {
        while read LINE; do
                        PKGNAME=`echo "${LINE}" | awk '{print $1}'`
                        DOT_PKG=`echo "${PKGNAME}" | grep ".pkg"`
                        if [ "x${DOT_PKG}" = "x" ]; then
                                BASEREV=`echo "${LINE}" | awk '{print $2}'`                     
                        else
                                BASEREV=`echo "${PKGNAME}" | awk -F- '{print $2}'`
                                BASEREV=`echo "${BASEREV}" | awk -F. '{print $1}'`
                                PKGNAME=`echo "${PKGNAME}" | awk -F- '{print $1}'`
                        fi
                        INSTALLED=`grep ${PKGNAME} ${ALLINSTALLEDPKG}`
                        if [ "a${INSTALLED}" = "a" ]; then
                                        NiceOutput_Error "${PKGNAME}" "Not installed"
                                        continue
                        fi

                        INSTALLED_APP=`echo ${INSTALLED} | awk '{print $2}'`
                        if [ "${INSTALLED_APP}" != "${PKGNAME}" ]; then
                                        NiceOutput_Error "${PKGNAME}" "Not installed"
                                        continue
                        fi

                        INSTALLEDREV=`pkginfo -l ${PKGNAME} | grep VERSION | awk '{print $2}'`
                        if [ "${BASEREV}" = "${INSTALLEDREV}" ]; then
                                        NiceOutput_OK "${PKGNAME}" 
                        else
                                get_latest ${BASEREV} ${INSTALLEDREV}
                                _GET_LATEST="${_RESULT}"
                                unset _RESULT
                                if [ "${_GET_LATEST}" = "${INSTALLEDREV}" ]; then       
                                        NiceOutput_Info "${PKGNAME}" "Later EC in system ${INSTALLEDREV} (Base revision at ${BASEREV})"
								else
                                        NiceOutput_Error "${PKGNAME}" "Upgrade to ${BASEREV} (Currently at ${INSTALLEDREV})"
                                fi                              
                        fi
        done < ${_BASE_FILE_NAME}
}

CleanUp() {
        rm ${ALLINSTALLEDPKG}
}

Check_Cmd_Line $*
GetAllInstalledPackages
CompareAllBasePackages
CleanUp